const getMessage1 = new Promise((resolve, reject) => {
    console.log(123)
})


const getMessage2 = async() => {
    console.log(456)
}


getMessage1
getMessage2() // promise


function foo() {
    console.log(this)
}


const bar = {
    callFoo: foo
}

foo() // window
bar.callFoo() // bar


const foo = () => {
    this
}
