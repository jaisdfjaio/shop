const createAddToCartButton = (productID) => {
  const $addToCartButton = $(
    `<button id="${productID}" class="add-to-cart -link btn btn-outline-primary btn-block">`
  ).html("加入購物車");
  $addToCartButton.on("click", (e) => {
    let quantity = $(`#${productID} .item-quantity`).val();
    console.log(productID)
    console.log(quantity);

  });
  return $addToCartButton;
};
